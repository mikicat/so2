#include <sem.h>
#include <sched.h>
#include <errno.h>

sem_t sem_list[NR_TASKS];
int sem_init(int n_sem, unsigned int value)
{
	if (n_sem < 0 || n_sem >= NR_TASKS) return -EACCES;
	if (sem_list[n_sem].init) return -EBUSY;
	sem_list[n_sem].count = value;
	INIT_LIST_HEAD(&(sem_list[n_sem].queue));
	sem_list[n_sem].init = 1;
	return 0;
}


int sem_wait(int n_sem)
{
	if (n_sem < 0 || n_sem >= NR_TASKS) return -EACCES;
	if (sem_list[n_sem].count <= 0)
	{
		// Bloquegem el procés (i TS)
		update_process_state_rr(current(), &(sem_list[n_sem].queue));
		sched_next_rr();
		// Retorna -1 si element de PCB detecta que ha sortit per sem_destroy
		if (current()->sem_destroyed)
		{
			current()->sem_destroyed = 0;
			return -1;
		}
	}
	else sem_list[n_sem].count--;
	return 0;
}


int sem_signal(int n_sem)
{
	if (n_sem < 0 || n_sem >= NR_TASKS) return -EACCES;
	sem_list[n_sem].count++;
	if (!list_empty(&sem_list[n_sem].queue))
	{
		struct list_head * e = list_first(&(sem_list[n_sem].queue));
		list_del(e);
		list_add_tail(e, &readyqueue);
	}
	return 0;
}

int sem_destroy(int n_sem)
{
	if (n_sem < 0 || n_sem >= NR_TASKS) return -EACCES;
	while (!list_empty(&(sem_list[n_sem].queue)))
	{
		struct list_head * e = list_first(&(sem_list[n_sem].queue));
		list_del(e);
		// Cal modificar paràmetre del PCB
		struct task_struct * PCB = list_head_to_task_struct(e);
		PCB->sem_destroyed = 1;
		list_add_tail(e, &readyqueue);
	}
	sem_list[n_sem].init = 0;
	return 1;
}
