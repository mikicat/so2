#include <libc.h>

char buff[24];

int pid;

void sigpipe_user_handler()
{
    write(1, "\nUSER\n", 6);
}

int __attribute__ ((__section__(".text.main")))
  main(void)
{
    /* Next line, tries to move value 0 to CR3 register. This register is a privileged one, and so it will raise an exception */
     /* __asm__ __volatile__ ("mov %0, %%cr3"::"r" (0) ); */
    sig_pipe(sigpipe_user_handler);
    int fd[2];
    pipe(fd);
    int f = fork();
    if (f == 0)
    {
        if (fork() == 0)
        {
            close(fd[1]);
            close(fd[0]);
	    // Es desbloqueja el Fill 1 per SIGPIPE
	    exit();
        }
        close(fd[0]);
        //int s_fd[2];
        //pipe(s_fd);
	// Provoquem un bloqueig pel write (omplim la pipe)
	/*for (int i = 0; i < 4098; ++i)
	{
		write(fd[1], "c", 1);
	}
        write(1, "passa", 5);
        write(fd[1], "Hola\n", 5);*/
        close(fd[1]); 
        exit();
    }
    close(fd[0]);
    close(fd[1]);
    char pidstr[4];
    pid = getpid();
    itoa(pid, pidstr);
    write(1, pidstr, 1);

  while(1) { }
}
