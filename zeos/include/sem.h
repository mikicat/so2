#ifndef __SEM_H__
#define __SEM_H__
#include <sched.h>


extern sem_t sem_list[NR_TASKS];

int sem_init(int n_sem, unsigned int value);
int sem_wait(int n_sem);
int sem_signal(int n_sem);
int sem_destroy(int n_sem);

#endif
