/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>

#include <utils.h>

#include <io.h>

#include <mm.h>

#include <mm_address.h>

#include <sched.h>

#include <p_stats.h>

#include <errno.h>

#include <libc.h>
#define LECTURA 0
#define ESCRIPTURA 1
// Definició de SIGPIPE. En general, si hagués d'implementar >1 signal,
// ho faria amb un fitxer similar a errno.h però com que només ens preocupa
// aquest senyal, el definiré aquí així
#define SIGPIPE 1

void * get_ebp();
void return_to_user_handler(unsigned int kernel_stack_end, void * usr_handler); 

int check_fd(int fd, int permissions)
{
  if (fd < 0 || fd >= NR_TASKS) return -EBADF; 
  else if (fd > 2)
  {
      if (current()->TC[fd].oft_entry == NULL) return -EBADF; // Comprovem que el Canal sigui obert
      if (current()->TC[fd].permisE != permissions) return -EACCES;
  }
  // Falta acabar

  return 0;
}

void user_to_system(void)
{
  update_stats(&(current()->p_stats.user_ticks), &(current()->p_stats.elapsed_total_ticks));
}

void system_to_user(void)
{
  update_stats(&(current()->p_stats.system_ticks), &(current()->p_stats.elapsed_total_ticks));
}

int sys_ni_syscall()
{
	return -ENOSYS; 
}

int sys_getpid()
{
	return current()->PID;
}

int global_PID=1000;

int ret_from_fork()
{
  return 0;
}

int sys_fork(void)
{
  struct list_head *lhcurrent = NULL;
  union task_union *uchild;
  
  /* Any free task_struct? */
  if (list_empty(&freequeue)) return -ENOMEM;

  lhcurrent=list_first(&freequeue);
  
  list_del(lhcurrent);
  
  uchild=(union task_union*)list_head_to_task_struct(lhcurrent);
  
  /* Copy the parent's task struct to child's */
  copy_data(current(), uchild, sizeof(union task_union));
  
  /* new pages dir */
  allocate_DIR((struct task_struct*)uchild);

  int pag, new_ph_pag, i;
  
  /* Allocate pages for DATA+STACK */
  page_table_entry *process_PT = get_PT(&uchild->task);
  for (pag=0; pag<NUM_PAG_DATA; pag++)
  {
    new_ph_pag=alloc_frame();
    if (new_ph_pag!=-1) /* One page allocated */
    {
      set_ss_pag(process_PT, PAG_LOG_INIT_DATA+pag, new_ph_pag);
    }
    else /* No more free pages left. Deallocate everything */
    {
      /* Deallocate allocated pages. Up to pag. */
      for (i=0; i<pag; i++)
      {
        free_frame(get_frame(process_PT, PAG_LOG_INIT_DATA+i));
        del_ss_pag(process_PT, PAG_LOG_INIT_DATA+i);
      }
      /* Deallocate task_struct */
      list_add_tail(lhcurrent, &freequeue);
      
      /* Return error */
      return -EAGAIN; 
    }
  }

  page_table_entry *parent_PT = get_PT(current());
  /* Allocate pages for PIPES */
  for (i=0; i < NR_TASKS; ++i)
  {
      if (current()->TC[i].oft_entry != NULL && current()->TC[i].permisE)
      {
          pag = (unsigned int)((current()->TC[i].oft_entry)->fitxer) >> 12;
          /* Copiem l'espai d'adreces per a pipes  */
          set_ss_pag(process_PT, pag, get_frame(parent_PT, pag));
          (current()->TC[i].oft_entry)->nrefsE++;
          (current()->TC[i].oft_entry)->nrefsL++;
      }
  }

  /* Copy parent's SYSTEM and CODE to child. */
  for (pag=0; pag<NUM_PAG_KERNEL; pag++)
  {
    set_ss_pag(process_PT, pag, get_frame(parent_PT, pag));
  }
  for (pag=0; pag<NUM_PAG_CODE; pag++)
  {
    set_ss_pag(process_PT, PAG_LOG_INIT_CODE+pag, get_frame(parent_PT, PAG_LOG_INIT_CODE+pag));
  }
  /* Copy parent's DATA to child. We will use TOTAL_PAGES-1 as a temp logical page to map to */
  for (pag=NUM_PAG_KERNEL+NUM_PAG_CODE; pag<NUM_PAG_KERNEL+NUM_PAG_CODE+NUM_PAG_DATA; pag++)
  {
    /* Map one child page to parent's address space. */
    set_ss_pag(parent_PT, pag+NUM_PAG_DATA, get_frame(process_PT, pag));
    copy_data((void*)(pag<<12), (void*)((pag+NUM_PAG_DATA)<<12), PAGE_SIZE);
    del_ss_pag(parent_PT, pag+NUM_PAG_DATA);
  }
  /* Copy PIPES to child */ 
  // Per simplicitat, assumeixo que nomes 1 pipe esta oberta (de moment) 
  set_ss_pag(parent_PT, pag+NUM_PAG_DATA, get_frame(process_PT, TOTAL_PAGES-1));

  /* Deny access to the child's memory space */
  set_cr3(get_DIR(current()));

  uchild->task.PID=++global_PID;
  uchild->task.state=ST_READY;

  int register_ebp;		/* frame pointer */
  /* Map Parent's ebp to child's stack */
  register_ebp = (int) get_ebp();
  register_ebp=(register_ebp - (int)current()) + (int)(uchild);

  uchild->task.register_esp=register_ebp + sizeof(DWord);

  DWord temp_ebp=*(DWord*)register_ebp;
  /* Prepare child stack for context switch */
  uchild->task.register_esp-=sizeof(DWord);
  *(DWord*)(uchild->task.register_esp)=(DWord)&ret_from_fork;
  uchild->task.register_esp-=sizeof(DWord);
  *(DWord*)(uchild->task.register_esp)=temp_ebp;

  /* Set stats to 0 */
  init_stats(&(uchild->task.p_stats));

  /* Queue child process into readyqueue */
  uchild->task.state=ST_READY;
  list_add_tail(&(uchild->task.list), &readyqueue);
  
  return uchild->task.PID;
}

int sys_close(int fd) {
    // Decrementem el nombre de referències
    // Si l'entrada a la TFO queda buida, podem eliminar la memòria reservada
    // Tancar el canal de la TC
    // Vigilar amb els semàfors
    struct task_struct * PCB = current();
    if (fd < 0 || fd >= NR_TASKS) return -ENXIO;
    opened_files_table_entry * e = PCB->TC[fd].oft_entry;
    if (e == NULL) return -ENXIO;
    if (PCB->TC[fd].permisE) e->nrefsE--;
    else e->nrefsL--;
    
    // S'ha alliberat la pipe
    if (e->nrefsE == 0 && e->nrefsL == 0)
    {
        // Alliberem pàgina fisica
        free_frame(get_frame(get_PT(PCB), ((unsigned int)(e->fitxer)>>12)));
        // Desassociem pàgina lògica
        del_ss_pag(get_PT(PCB), ((unsigned int)(e->fitxer)>>12));
        e->fitxer = NULL;
    }
    else if (e->nrefsE == 0)
    {
        // Alliberem processos bloquejats per la pipe
	while (!list_empty(&(e->semL.queue)))
	{
		struct list_head * p = list_first(&(e->semL.queue));
		list_del(p);
		list_add_tail(p, &readyqueue);
	}
    }
    else if (e->nrefsL == 0)
    {
        // Alliberem processos bloquejats per la pipe
	while (!list_empty(&(e->semE.queue)))
	{
		struct list_head * p = list_first(&(e->semE.queue));
		list_del(p);
		// Cal modificar paràmetre del PCB
		struct task_struct * pPCB = list_head_to_task_struct(p);
                // SIGPIPE
                if (pPCB->treat_sigpipe) list_add_tail(p, &readyqueue);
                else 
                {
                    // Cal matar el procés si no es pot tractar
                      int i;
                      page_table_entry *process_PT = get_PT(pPCB);
                      // Tancar els canals que encara estiguin oberts a la TC
                      for (i = 0; i < NR_TASKS; ++i)
                      {
                          if (pPCB->TC[i].oft_entry != NULL)
                              sys_close(i);
                      }

                      // Deallocate all the propietary physical pages
                      for (i=0; i<NUM_PAG_DATA; i++)
                      {
                        free_frame(get_frame(process_PT, PAG_LOG_INIT_DATA+i));
                        del_ss_pag(process_PT, PAG_LOG_INIT_DATA+i);
                      }
                      
                      /* Free task_struct */
                      list_add_tail(&(pPCB->list), &freequeue);
                      pPCB->PID=-1;
                }
	}
    }
    PCB->TC[fd].oft_entry = NULL;
    return 0;
}

void sys_exit()
{  
  int i;

  page_table_entry *process_PT = get_PT(current());
  // Tancar els canals que encara estiguin oberts a la TC
  for (i = 0; i < NR_TASKS; ++i)
  {
      if (current()->TC[i].oft_entry != NULL)
          sys_close(i);
  }

  // Deallocate all the propietary physical pages
  for (i=0; i<NUM_PAG_DATA; i++)
  {
    free_frame(get_frame(process_PT, PAG_LOG_INIT_DATA+i));
    del_ss_pag(process_PT, PAG_LOG_INIT_DATA+i);
  }
  
  /* Free task_struct */
  list_add_tail(&(current()->list), &freequeue);
  
  current()->PID=-1;
  
  /* Restarts execution of the next process */
  sched_next_rr();
}

int handle_signal(int signal)
{
    if (signal == SIGPIPE)
    {
        if (current()->handler_sigpipe == NULL)
        {
            // Handler per defecte
            printk("Process ");
            char s[4];
            itoa(current()->PID, s);
            printk(s);
            printk(" received a SIGPIPE signal\n");
        }
        else
        {
            return_to_user_handler(KERNEL_ESP((union task_union *)current()), current()->handler_sigpipe);
        }
        return -EPIPE;
    }
    return 0;
}

#define TAM_BUFFER 512

int sys_write(int fd, char *buffer, int nbytes) {
char localbuffer [TAM_BUFFER];
int bytes_left;
int ret;

	if ((ret = check_fd(fd, ESCRIPTURA)))
		return ret;
	if (nbytes < 0)
		return -EINVAL;
	if (!access_ok(VERIFY_READ, buffer, nbytes))
		return -EFAULT;
	
	bytes_left = nbytes;
        opened_files_table_entry * e = current()->TC[fd].oft_entry; // punter a la TFO
        if (fd < 3) {
            // Assumim que no es pot tancar i reobrir canals estandard -de moment-
            while (bytes_left > TAM_BUFFER) {
                    copy_from_user(buffer, localbuffer, TAM_BUFFER);
                    ret = sys_write_console(localbuffer, TAM_BUFFER);
                    bytes_left-=ret;
                    buffer+=ret;
            }
            if (bytes_left > 0) {
                    copy_from_user(buffer, localbuffer,bytes_left);
                    ret = sys_write_console(localbuffer, bytes_left);
                    bytes_left-=ret;
            }
        }
        else {
            int t_bytes = nbytes;
            if (e->availbytes + nbytes > PAGE_SIZE) nbytes = PAGE_SIZE - e->availbytes;
            // Si tenim espai per escriure
            if (nbytes > 0) 
            {
                // Calculem el punter d'escriptura a partir del de lectura i dels bytes ja escrits al buffer
                void * punterE = e->punterL + e->availbytes;
                if (punterE - e->fitxer >= PAGE_SIZE) punterE -= PAGE_SIZE;
                // Estructura circular: quan arribem a l'extrem tornem a l'inici
                int overflow = punterE + nbytes - e->fitxer;
                if (overflow >= PAGE_SIZE) {
                    // Escrivim fins al final de la pàgina
                    int final = e->fitxer+PAGE_SIZE-punterE;
                    copy_from_user(buffer, punterE, final);
                    // I escrivim des del principi de la pipe fins a escriure tot el que queda
                    copy_from_user(buffer+final, e->fitxer, nbytes-final);
                }
                else {
                    // Si no hi ha overflow, podem escriure els nbytes
                    copy_from_user(buffer, punterE, nbytes);
                }
                e->availbytes += nbytes;
                // Alliberem processos bloquejats per la pipe
                while (!list_empty(&(e->semL.queue)))
                {
                        struct list_head * p = list_first(&(e->semL.queue));
                        list_del(p);
                        // Cal modificar paràmetre del PCB
                        //struct task_struct * pPCB = list_head_to_task_struct(p);
                        list_add_tail(p, &readyqueue);
                }
            }
            if (t_bytes != nbytes && e->nrefsL > 0) 
            {
                // No tenim espai per escriure
                // Bloquegem el procés (i TS)
                printk("Em bloquejo al write\n");
                update_process_state_rr(current(), &(e->semE.queue));
                sched_next_rr();
                if (e->nrefsL == 0)
                {
                    // Despertat perquè no hi ha lectors i tenim tractament
		    // S'ha rebut un SIGPIPE
                    return handle_signal(SIGPIPE);
                }
                else sys_write(fd, buffer+nbytes, t_bytes-nbytes);
            }
        }
	return (nbytes-bytes_left);
}

int sys_read(int fd, char *buffer, int size){
int bytes_left;
int ret;

    if ((ret = check_fd(fd, LECTURA))) 
            return ret;
    if (size < 0)
            return -EINVAL;
    /*if (!access_ok(VERIFY_READ, buffer, nbytes))
            return -EFAULT;
    */
    int t_size = size;
    opened_files_table_entry * e = current()->TC[fd].oft_entry; // punter a la TFO
    bytes_left = e->availbytes;
    // Comprovem si tenim alguna cosa a llegir
    if (e->availbytes > 0) {
        // No ens passem si llegim mes del disponible
        if (size > e->availbytes) size = e->availbytes;
        // Estructura circular: quan arribem al final tornem a l'inici
        int overflow = e->punterL + size - e->fitxer;
        if (overflow >= PAGE_SIZE) {
            // Llegim fins al final de la pàgina
            int final = e->fitxer+PAGE_SIZE-(e->punterL);
            copy_to_user(e->punterL, buffer, final);
            e->punterL = e->fitxer;
            // Llegim des del principi de la pipe fins on haguem de llegir
            copy_to_user(e->punterL, buffer+final, size-final);
            e->punterL += size-final;
        }
        else {
            // Si no hi ha overflow, ho llegim tot de cop
            copy_to_user(e->punterL, buffer, size);
            e->punterL += size;
        }
        e->availbytes -= size;
        // Desbloquejar escriptors
	while (!list_empty(&(e->semE.queue)))
	{
		struct list_head * p = list_first(&(e->semE.queue));
		list_del(p);
		// Cal modificar paràmetre del PCB
		//struct task_struct * pPCB = list_head_to_task_struct(p);
		list_add_tail(p, &readyqueue);
	}
    }
    if ((t_size != size || bytes_left == 0) && e->nrefsE > 0){
        // Do stuff
        // Bloquegem el procés (i TS)
        printk("Em bloquejo\n");
        update_process_state_rr(current(), &(e->semL.queue));
        sched_next_rr();
        if (e->availbytes > 0)
        {
            if (t_size != size) sys_read(fd, buffer+size, t_size-bytes_left);
            else sys_read(fd, buffer, t_size-bytes_left);
        }
        else return 0;

    }
    return (t_size);
}

extern int zeos_ticks;

int sys_gettime()
{
  return zeos_ticks;
}


/* System call to force a task switch */
int sys_yield()
{
  force_task_switch();
  return 0;
}

extern int remaining_quantum;

int sys_get_stats(int pid, struct stats *st)
{
  int i;
  
  if (!access_ok(VERIFY_WRITE, st, sizeof(struct stats))) return -EFAULT; 
  
  if (pid<0) return -EINVAL;
  for (i=0; i<NR_TASKS; i++)
  {
    if (task[i].task.PID==pid)
    {
      task[i].task.p_stats.remaining_ticks=remaining_quantum;
      copy_to_user(&(task[i].task.p_stats), st, sizeof(struct stats));
      return 0;
    }
  }
  return -ESRCH; /*ESRCH */
}


int sys_pipe(int *fd) {
    // Trobar pag log buida (després de dades)
    // assignar pag fis a la pagina logica
    // actualitzar TC i TFO
    struct task_struct * PCB = current();
    page_table_entry *process_PT = get_PT(PCB);
    int pfis = alloc_frame();
    if (pfis < 0) return -EAGAIN; /* No pag fis disponibles */
    int chR = 3; // Canals 0 i 1 i 2 reservats per stdin i stdout i stderr
    int chW = 3;
    int found = 0;
    while (chR < NR_TASKS && chW < NR_TASKS && found < 2)
    {
        if (found == 0) {
            if (PCB->TC[chR].oft_entry == NULL){
                chW++;
                found++;
            }
            else {
                chR++, chW++;
            }
        }
        else {
            if (PCB->TC[chW].oft_entry == NULL) {
                found++;
            }
            else chW++;
        }
    }
    if (found < 2) return -1; /* No hi ha entrades a la TC disponibles */
    int fitxer = 0;
    found = 0;
    while (fitxer < NR_TASKS*NR_TASKS && !found)
    {
        if (opened_files_table[fitxer].fitxer == NULL){
            found++;
        }
        else {
            fitxer++;
        }
    }
    if (!found) return -1; /* No hi ha entrades a la TFO disponibles */

    // Busquem una pàgina lògica lliure
    unsigned int plog;
    for (plog = TOTAL_PAGES-1; plog > PAG_LOG_INIT_DATA+NUM_PAG_DATA; --plog)
    {
        if (process_PT[plog].entry == 0)
        {
            break;
        }
    }
    if (plog == PAG_LOG_INIT_DATA+NUM_PAG_DATA) return -EAGAIN; /* No hi ha p log disponibles */
    // Associem p lògica amb p física
    set_ss_pag(process_PT, plog, pfis);

    // Associem entrades TC a entrades TFO corresponents
    PCB->TC[chR].oft_entry = &opened_files_table[fitxer];
    PCB->TC[chR].permisE = 0;
    PCB->TC[chW].oft_entry = &opened_files_table[fitxer];
    PCB->TC[chW].permisE = 1;
    
    // Inicialitzem entrada TFO
    opened_files_table[fitxer].nrefsE = 1;
    opened_files_table[fitxer].nrefsL = 1;
    opened_files_table[fitxer].punterL = plog << 12;
    opened_files_table[fitxer].availbytes = 0;
    opened_files_table[fitxer].fitxer = plog << 12;
    // Init sem Lectura
    INIT_LIST_HEAD(&(opened_files_table[fitxer].semL.queue));
    opened_files_table[fitxer].semL.init = 1;
    opened_files_table[fitxer].semL.count = 1;
    // Init sem Escriptura
    INIT_LIST_HEAD(&(opened_files_table[fitxer].semE.queue));
    opened_files_table[fitxer].semE.init = 1;
    opened_files_table[fitxer].semE.count = 1;
    // Assignar els file descriptors (canals) corresponents
    fd[0] = chR; // Lectura 
    fd[1] = chW; // Escriptura

    return 0;
}

void sys_sig_pipe(void (*user_function)(void))
{
    current()->treat_sigpipe = 1;
    current()->handler_sigpipe = user_function;
}
